create database practicedb;




use  practicedb

create table Worker1(Worker_id int primary key,
first_name char(20) not Null,
last_name char(20),
salary int check (salary between 10000 and 250000) ,
Joining_date datetime,
dept char(20) check( dept in ('hr','sales','accts','it')));


insert into  worker1 values (1,'Moksh','Sethi',25000,'2023-05-01 12:40:12','IT');
insert into  worker1 values (2,'shivam','dhaka',21000,'2023-05-01 12:40:12','accts');
insert into  worker1 values (3,'jatin','bhola',23000,'2023-05-01 12:40:12','hr');
insert into  worker1 values (4,'anusha','goel',20000,'2023-05-01 12:40:12','accts');
insert into  worker1 values (5,'Muskan','dembla',20000,'2023-05-01 12:40:12','hr');
insert into  worker1 values (6,'parul','chandra',19000,'2023-05-01 12:40:12','it');
insert into  worker1 values (7,'himanshu','rawat',21110,'2023-05-01 12:40:12','hr');
insert into  worker1 values (8,'akanshi','gupta',22300,'2023-05-01 12:40:12','accts');
insert into  worker1 values (9,'karan','singh',18000,'2023-05-01 12:40:12','hr');

select * from worker1;

select first_name as Worker_name from worker1;

select upper(first_name) from worker1;

select distinct(dept) from worker1;

SELECT SUBSTRING(first_name, 1, 3) AS ExtractString
FROM worker1;

SELECT CHARINDEX('m', 'karan') As first_name;

select rtrim(first_name) from worker1;

select ltrim(dept) from worker1;

select * from worker1;

 select distinct (dept),len (dept) as deptlen from worker1;

 select  replace (first_name,'m','M') from worker1;

 select concat(first_name,'',last_name)as full_name from worker1;

 select * from worker1 order by first_name Asc;

  select * from worker1 order by first_name Asc , dept desc;

  select * from worker1 where first_name in('moksh','karan');

  
  select * from worker1 where dept in( 'admin');

  select * from worker1 where first_name like '%a%';

  select * from worker1 where first_name like '%a';

  select * from worker1 where first_name like '____h ';

  select * from worker1 where salary between 15000 and 30000;

  select * from worker1 where month(Joining_date)=05 and year(Joining_date)=2023;

  select count (first_name) as worker_in_hr from worker1 where dept like'hr';

